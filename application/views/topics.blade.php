@layout('layout.master')

@section('content')
<h2>{{$topic->forum()->name}}</h2>
<table>
    <th></th>
    @foreach($posts as $post)
    <tr>
        <td>{{ HTML::link('u/' . $post->user_id, $post->username); }}</td>
        <td>{{ $post->body }}</td>
    </tr>
    @endforeach
</table>
@endsection

@section('title')
{{ $topic->title }}
@endsection

