@layout('layout.master')

@section('content')
<h2>{{ $topic->title }}</h2>
<table>
    <th></th>
    @foreach($posts->results as $post)
    <tr>
        <td>{{ HTML::link('u/' . $post->user_id, $post->username); }}</td>
        <td>{{ $post->body }}</td>
    </tr>
    @endforeach
</table>


    <div>
        {{ Form::open('/newpost', 'POST') }}
        {{ Form::hidden('topic_id', $topic->id) }}
        {{ Form::hidden('forum_id', $topic->forum_id) }}
        {{ Form::hidden('user_id', $topic->user_id) }}

        {{ Form::label('body', 'Content') }}
        {{ Form::textarea('body') }}<br />

        {{ Form::submit('Post') }}

        {{ Form::close() }}
    </div>

{{ $posts->links() }}
@endsection

@section('title')
{{ $topic->title . ' - ' . $topic->forum()->name }}
@endsection

