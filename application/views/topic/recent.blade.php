@layout('layout.master')

@section('content')
<h2> Recent Topics </h2>
<table>
    <th></th>
    @foreach($topics->results as $topic)
    <tr>
        <td>{{ HTML::link('u/' . $topic->user_id, $topic->username . ' >> ' . $topic->name ); }}</td>
        <td>{{ $topic->title }}</td>
    </tr>
    @endforeach
</table>
{{ $topics->links() }}
@endsection

@section('title')
	Recent
@endsection

