@layout('layout.master')

@section('content')
<h2> New Topic </h2>
	{{ Form::open('/newtopic', 'POST') }}
	
	{{ Form::label('title', 'Topic Title') }}
	{{ Form::text('title') }}<br />

	{{ Form::label('forum', 'Forum') }}
	{{ Form::select('forum', $forums) }}<br />
	
	{{ Form::label('post', 'Content') }}
	{{ Form::textarea('post') }}<br />

	{{ Form::submit('Post') }}
	
	{{ Form::close() }}
@endsection

@section('title')
	New Topic
@endsection

