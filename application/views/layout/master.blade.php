<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>@yield('title')</title>
</head>
<body>
    <header>
        <h1>TinyBB</h1>
        <h2>A Tiny Bulletin Board</h2>
    </header>
    <div>
        <ul>
            <li>{{HTML::link('/', 'Home');}}</li>
            <li>{{HTML::link('recent', 'Recent Topics');}}</li>
            @yield('navbar')
        </ul>
    </div>

    @if($errors)
    <div class="error_message">
        {{ $errors->all('<p>:message</p>') }}
    </div>
    @endif

    <div class="content">
    @yield('content')
    </div>

    @yield('editor')
    
    <footer>
        My Tiny Forum
    </footer>
</body>
</html>