@layout('layout.master')

@section('content')
<h2>{{$forum->name}}</h2>
<table>
    <th></th>
    @foreach($topics->results as $topic)
    <tr>
        <td>{{ HTML::link('t/' . $topic->id, $topic->title); }}</td>
        <td>{{ $topic->username }}</td>
    </tr>
    @endforeach
</table>

    {{ $topics->links() }}
@endsection

@section('title')
{{ $forum->name }}
@endsection

