@layout('layout.master')

@section('content')

<table>
    <th>
        <td>Forum</td>
        <td>Topics</td>
    </th>
    @foreach($forums as $forum)
    <tr>
        <td>{{ HTML::link_to_route('show_forum', $forum->name, array('id' => $forum->id)); }}</td>
        <td>{{ $forum->topics_count() }}</td>
    </tr>
    @endforeach
</table>
@endsection

@section('title')
{{"Forum home"}}
@endsection

