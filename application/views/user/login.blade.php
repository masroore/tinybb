@layout('layout.master')

@section('content')
<h2>Login</h2>

<div>
    {{ Form::open('/dologin', 'POST') }}

    {{ Form::label('username', 'User Name') }}
    {{ Form::text('username') }}<br />

    {{ Form::label('password', 'Password') }}
    {{ Form::text('password') }}

    {{ Form::submit('Login') }}

    {{ Form::close() }}
</div>

@endsection

@section('title')
Login
@endsection

