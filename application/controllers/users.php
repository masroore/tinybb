<?php

class Users_Controller extends Base_Controller
{
    public $restful = true;

    public function get_login()
    {
        return View::make('user.login');
    }

    public function post_dologin()
    {
        $rules = array(
            'username' => 'required',
            'password' => 'required',
        );

        $input = Input::all();

        $v = Validator::make($input, $rules);
        if ($v->fails()) {
            return Redirect::to('login')
                ->with_errors($v);
        }

        $credentials = array(
            'username' => Input::get('username'),
            'password' => Input::get('password')
        );

        if (Auth::attempt($credentials)) {
            return Redirect::to('/');
        } else {
            return Redirect::to('login')
                ->with_errors($v);
        }
    }
}