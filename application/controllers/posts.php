<?php

class Posts_Controller extends Base_Controller
{
    public $restful = true;

    public function post_newpost()
    {
        $rules = array(
            'forum_id' => 'required|integer',
            'topic_id' => 'required|integer',
            'user_id' => 'required|integer',
            'body' => 'required'
        );

        $input = Input::all();
        $topic_id = Input::get('topic_id');

        $v = Validator::make($input, $rules);
        if ($v->fails()) {
            return Redirect::to_action('topics@index', array($topic_id))
                ->with('error', $v->errors);
        }

        Post::create(array(
            'topic_id' => $topic_id,
            'forum_id' => Input::get('forum_id'),
            'user_id' => Input::get('user_id'),
            'body' => Input::get('body'),
        ));

        return Redirect::to_action('topics@index', array($topic_id));
    }
}