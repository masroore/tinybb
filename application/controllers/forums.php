<?php

class Forums_Controller extends Base_Controller
{

    public function action_index()
    {
        $forums = Forum::list_forums();
        return View::make('forum.index', array('forums' => $forums));
    }

    public function action_show($id)
    {
        $forum = Forum::find($id);
        if ($forum) {
            $topics = Topic::list_topics_in_forum($forum->id);
            return View::make('forum.show',
                array(
                    'forum' => $forum,
                    'topics' => $topics
                )
            );
        }
        
        return Redirect::to_action('forums@index')
        ->with('error', 'Forum not found!');
    }

    public function action_new()
    {

    }
}