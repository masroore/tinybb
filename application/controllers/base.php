<?php

class Base_Controller extends Controller
{
    public function __construct()
    {
        parent::__construct();

        //Filters
        $class = get_called_class();
        switch ($class) {
            case 'Forums_Controller':
                $this->filter('before', 'nonauth');
                return;
            case 'Posts_Controller':
                $this->filter('before', 'auth')->only(array('newpost'));
                return;
            case 'Topics_Controller':
                $this->filter('before', 'auth')->only(array('newtopic'));
                return;
        }
    }

    public function __call($method, $parameters)
    {
        return Response::error('404');
    }

}