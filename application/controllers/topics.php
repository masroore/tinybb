<?php

class Topics_Controller extends Base_Controller
{
    public static $restful = true;

    public function get_index($id)
    {
        $topic = Topic::find($id);
        $posts = Post::list_posts_in_topic($topic->id);
        return View::make('topic.index', array('posts' => $posts, 'topic' => $topic));
    }

    public function get_recent()
    {
        $topics = Topic::list_recent_topics();
        return View::make('topic.recent', array('topics' => $topics));
    }

    public function get_new()
    {
        $forums = Forum::get_forums_array();
        return View::make('topic.new', array('forums' => $forums));
    }

    public function post_new()
    {
        Topic::validate();
        
        $topic = Topic::create(array(
            'forum_id' => Input::get('forum'),
            'user_id' => 1,
            'title' => Input::get('title'),
            'hits' => 1,
        ));

        Post::create(array(
            'topic_id' => $topic->id,
            'forum_id' => $topic->forum_id,
            'user_id' => $topic->user_id,
            'body' => Input::get('post'),
            ));

        return Redirect::to_action('topics@index', array($topic->id));
    }
}