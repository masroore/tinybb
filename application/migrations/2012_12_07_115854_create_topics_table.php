<?php

class Create_Topics_Table
{
    public function up()
    {
        Schema::create('topics', function ($t) {
                $t->increments('id');
                $t->integer('forum_id')->unsigned();
                $t->integer('user_id')->unsigned();
                $t->string('title', 100);
                $t->integer('hits')->default(0);
                $t->boolean('locked')->default(0);
                $t->boolean('sticky')->default(0);
                $t->timestamps();
                $t->index(array('forum_id', 'user_id', 'created_at'));
            }
        );

        Topic::create(array(
            'id' => 1,
            'forum_id' => 1,
            'user_id' => 1,
            'title' => 'Topic #1',
            'hits' => 0,
            'locked' => false,
            'sticky' => false
        ));

        Topic::create(array(
            'id' => 2,
            'forum_id' => 1,
            'user_id' => 2,
            'title' => 'Topic #2',
            'hits' => 0,
            'locked' => false,
            'sticky' => false
        ));

        Topic::create(array(
            'id' => 3,
            'forum_id' => 2,
            'user_id' => 3,
            'title' => 'Topic #3',
            'hits' => 0,
            'locked' => false,
            'sticky' => false
        ));

        Topic::create(array(
            'id' => 4,
            'forum_id' => 2,
            'user_id' => 3,
            'title' => '[Locked] Topic #4',
            'hits' => 0,
            'locked' => true,
            'sticky' => false
        ));

        Topic::create(array(
            'id' => 5,
            'forum_id' => 2,
            'user_id' => 3,
            'title' => '[Sticky] Topic #4',
            'hits' => 0,
            'locked' => false,
            'sticky' => true
        ));
    }

    public function down()
    {
        Schema::drop('topics');
    }

}