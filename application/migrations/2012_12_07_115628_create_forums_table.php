<?php

class Create_Forums_Table
{

    /**
     * Make changes to the database.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('forums', function ($t) {
                $t->increments('id');
                $t->string('name', 100);
                $t->text('description')->nullable();
                $t->integer('position');
                $t->timestamps();
                $t->index(array('position', 'name'));
            }
        );

        Forum::create(array(
            'name' => 'Forum #1',
            'description' => 'Description for forum 1',
            'position' => 1
        ));

        Forum::create(array(
            'name' => 'Forum #2',
            'description' => 'Description for forum 2',
            'position' => 1
        ));

        Forum::create(array(
            'name' => 'Forum #3',
            'description' => 'Description for forum 3',
            'position' => 0
        ));
    }

    /**
     * Revert the changes to the database.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('forums');
    }
}