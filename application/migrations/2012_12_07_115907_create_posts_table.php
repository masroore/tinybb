<?php

class Create_Posts_Table
{
    public function up()
    {
        Schema::create('posts', function ($t) {
                $t->increments('id');
                $t->integer('forum_id')->unsigned();
                $t->integer('topic_id')->unsigned();
                $t->integer('user_id')->unsigned();
                $t->text('body');
                $t->timestamps();
                $t->index(array('forum_id', 'topic_id', 'user_id', 'created_at'));
            }
        );

        Post::create(array(
            'id' => 1,
            'forum_id' => 1,
            'topic_id' => 1,
            'user_id' => 1,
            'body' => 'Post #1'
        ));

        Post::create(array(
            'id' => 2,
            'forum_id' => 1,
            'topic_id' => 1,
            'user_id' => 1,
            'body' => 'Post #2'
        ));

        Post::create(array(
            'id' => 3,
            'forum_id' => 2,
            'topic_id' => 5,
            'user_id' => 3,
            'body' => 'Post #3'
        ));
    }

    public function down()
    {
        Schema::drop('posts');
    }
}