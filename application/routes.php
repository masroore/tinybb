<?php
/*Route::get('/',

function()
{
    $forum = Forum::find(1);
    //dd($forum->topics_count());
	//dd(Topic::list_topics_in_forum(1));
	//dd($forum);
	$topic = $forum->last_topic();
	dd($topic);
	dd($topic->user()->only('username'));
}
	);
return;
*/

Route::controller(Controller::detect());

Route::get('/', 'forums@index');

Route::get('f/(:num)', array('as' => 'show_forum', 'uses' => 'forums@show'));

Route::get('t/(:num)', array('as' => 'show_topic', 'uses' => 'topics@index'));

Route::get('recent', array('as' => 'show_recent', 'uses' => 'topics@recent'));

Route::group(array('before' => 'auth'), function () {

    Route::get('newtopic', array('as' => 'get_newtopic', 'uses' => 'topics@new'));

    Route::post('newtopic', array('as' => 'post_newtopic', 'uses' => 'topics@new'));

    Route::post('newpost', array('as' => 'post_new', 'uses' => 'posts@newpost'));
});

Route::get('login', array('as' => 'login_now', 'uses' => 'users@login'));

Route::post('dologin', array('as' => 'dologin', 'uses' => 'users@dologin'));

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Simply tell Laravel the HTTP verbs and URIs it should respond to. It is a
| breeze to setup your application using Laravel's RESTful routing and it
| is perfectly suited for building large applications and simple APIs.
|
| Let's respond to a simple GET request to http://example.com/hello:
|
|		Route::get('hello', function()
|		{
|			return 'Hello World!';
|		});
|
| You can even respond to more than one URI:
|
|		Route::post(array('hello', 'world'), function()
|		{
|			return 'Hello World!';
|		});
|
| It's easy to allow URI wildcards using (:num) or (:any):
|
|		Route::put('hello/(:any)', function($name)
|		{
|			return "Welcome, $name.";
|		});
|
*/

/*Route::get('/', function()
{
	return View::make('home.index');
});*/

/*
|--------------------------------------------------------------------------
| Application 404 & 500 Error Handlers
|--------------------------------------------------------------------------
|
| To centralize and simplify 404 handling, Laravel uses an awesome event
| system to retrieve the response. Feel free to modify this function to
| your tastes and the needs of your application.
|
| Similarly, we use an event to handle the display of 500 level errors
| within the application. These errors are fired when there is an
| uncaught exception thrown in the application.
|
*/

Event::listen('403', function ($message = 'Forbidden') {
    return Response::error('403', array('message' => $message));
});

Event::listen('404', function () {
    return Response::error('404');
});

Event::listen('500', function () {
    return Response::error('500');
});

/*
|--------------------------------------------------------------------------
| Route Filters
|--------------------------------------------------------------------------
|
| Filters provide a convenient method for attaching functionality to your
| routes. The built-in before and after filters are called before and
| after every request to your application, and you may even create
| other filters that can be attached to individual routes.
|
| Let's walk through an example...
|
| First, define a filter:
|
|		Route::filter('filter', function()
|		{
|			return 'Filtered!';
|		});
|
| Next, attach the filter to a route:
|
|		Router::register('GET /', array('before' => 'filter', function()
|		{
|			return 'Hello World!';
|		}));
|
*/

Route::filter('before', function () {
    // Do stuff before every request to your application...
});

Route::filter('after', function ($response) {
    // Do stuff after every request to your application...
});

Route::filter('csrf', function () {
    if (Request::forged()) return Response::error('500');
});

Route::filter('auth', function () {
    if (Auth::guest()) {
        return Event::first('403', 'Logged in users only.');
    }
});