<?php

class Topic extends Eloquent
{
    const TOPICS_PER_PAGE = 10;

    private static $_rules = array(
        'title' => 'required|max:100',
        'forum' => 'required|integer',
        'post' => 'required'
    );

    public static function validate()
    {
        $input = Input::all();
        $v = Validator::make($input, static::_rules);

        if ($v->fails()) {
            return Redirect::to('/new')->with('error', $v->errors);
        }
    }

    public function forum()
    {
        return $this->belongs_to('Forum')->first();
    }

    public function posts()
    {
        return $this->has_many('Post')->first();
    }

    public function user()
    {
        return $this->belongs_to('User');
    }

    public static function list_topics_in_forum($forum_id)
    {
        return DB::table('topics')
            ->join('users', 'users.id', '=', 'topics.user_id')
            ->where('topics.forum_id', '=', $forum_id)
            ->order_by('created_at', 'DESC')
        //->skip($offset)->take($rows)
            ->paginate(Topic::TOPICS_PER_PAGE, array('users.username', 'topics.*'));
    }

    public static function list_recent_topics()
    {
        return DB::table('topics')
            ->join('users', 'users.id', '=', 'topics.user_id')
            ->join('forums', 'forums.id', '=', 'topics.forum_id')
            ->order_by('created_at', 'DESC')
        //->skip($offset)->take($rows)
            ->paginate(Topic::TOPICS_PER_PAGE, array('users.username', 'forums.name', 'topics.*'));
    }

    public function posts_count()
    {
        return Post::where('topic_id', '=', $this->id)->count();
    }
}
