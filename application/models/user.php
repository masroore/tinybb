<?php

class User extends Eloquent
{
    public static $timestamps = true;

    public function roles()
    {
        return $this->has_many_and_belongs_to('Role');
    }

    public function topics()
    {
        return $this->has_many('Topic');
    }

    public function posts()
    {
        return $this->has_many('Post');
    }

    public function has_role($key)
    {
        foreach (Auth::user()->roles() as $role) {
            if (strcmp($role->key, $key) == 0) {
                return true;
            }
        }
        return false;
    }

    public function has_any_roles($keys)
    {
        if (!is_array($keys)) {
            $keys = func_get_args();
        }

        foreach (Auth::user()->roles() as $role) {
            if (in_array($role->key, $keys))
                return true;
        }
        return false;
    }

    public function validate_and_insert()
    {
        $this->rules['password'] = 'required';
        $validator = new Validator(Input::all(), $this->rules);

        if ($validator->valid()) {
            $this->email = Input::get('email');
            $this->password = Hash::make(Input::get('password'));
            $this->username = Input::get('username');
            $this->display_name = Input::get('display_name');
            $this->save();

            if (Input::has('role_ids')) {
                foreach (Input::get('role_ids') as $role_id) {
                    if ($role_id == 0) continue;

                    DB::table('roles_users')
                        ->insert(array('user_id' => $this->id, 'role_id' => $role_id));
                }
            }
        }

        return $validator->errors;
    }

    public function validate_and_update()
    {
        $validator = new Validator(Input::all(), $this->rules);
        if ($validator->valid()) {
            DB::table('roles_users')->where_user_id($this->id)->delete();

            if (Input::has('role_ids')) {
                foreach (Input::get('role_ids') as $role_id) {
                    if ($role_id == 0) continue;

                    DB::table('roles_users')
                        ->insert(array('user_id' => $this->id, 'role_id' => $role_id));
                }
            }

            $this->email = Input::get('email');
            if ($password = Input::get('password')) $this->password = Hash::make($password);
            $this->username = Input::get('username');
            $this->display_name = Input::get('display_name');
            $this->save();
        }

        return $validator->errors;
    }
}
