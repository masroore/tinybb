<?php

class Forum extends Eloquent
{
    public static function list_forums()
    {
        return static::order_by('position')->get();
    }

    public static function get_forums_array()
    {
        $rows = static::order_by('position')->get(array('id', 'name'));
        $result = array();
        foreach ($rows as $row) {
            $result[$row->id] = $row->name;
        }
        return $result;
    }

    public function topics_count()
    {
        return Topic::where('forum_id', '=', $this->id)->count();
    }

    public function last_topic()
    {
        return DB::table('topics')
            ->join('users', 'users.id', '=', 'topics.user_id')
            ->where('topics.forum_id', '=', $this->id)
            ->order_by('topics.created_at', 'DESC')
            ->take(1)
            ->get(array(
            'topics.id AS topic_id',
            'topics.title',
            'topics.updated_at',
            'users.username',
            'topics.user_id',));
    }
}