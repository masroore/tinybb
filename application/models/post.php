<?php

class Post extends Eloquent
{
    const POSTS_PER_PAGE = 10;

    public function topic()
    {
        return $this->belongs_to('Topic');
    }

    public function user()
    {
        return $this->belongs_to('User');
    }

    public function forum()
    {
        return $this->belongs_to('Forum');
    }

    public static function list_posts_in_topic($topic_id)
    {
        return DB::table('posts')
            ->join('users', 'users.id', '=', 'posts.user_id')
            ->where('posts.topic_id', '=', $topic_id)
            ->order_by('created_at', 'ASC')
            //->skip($offset)->take($rows)
            ->paginate(Post::POSTS_PER_PAGE, array('users.username', 'posts.*'));
    }
}
