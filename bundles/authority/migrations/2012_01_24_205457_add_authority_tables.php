<?php

class Authority_add_authority_tables {

	public function up()
	{
		Schema::create('users', function($table)
		{
			$table->increments('id');
            $table->string('username', 50)->unique();
			$table->string('email', 50)->unique();
			$table->string('password',32);
            $table->timestamp('last_login_at')->nullable();
			$table->timestamps();
		});

		User::create(array(
			'id' => 1,
			'email' => 'admin@domain.com',
			'password' => Hash::make('password'),
			'username' => 'admin',
		));

		User::create(array(
			'id' => 2,
			'email' => 'moderator@domain.com',
			'password' => Hash::make('password'),
			'username' => 'moderator',
		));

		User::create(array(
			'id' => 3,
			'email' => 'user1@domain.com',
			'password' => Hash::make('password'),
			'username' => 'user1',
		));


		Schema::create('roles', function($table)
		{
			$table->increments('id');
			$table->string('name', 40)->unique();
			$table->timestamps();
		});

		Role::create(array(
			'id' => 1,
			'name' => 'administrator'
		));

		Role::create(array(
			'id' => 2,
			'name' => 'moderator'
		));

        Role::create(array(
			'id' => 3,
			'name' => 'user'
		));

		Schema::create('role_user', function($table)
		{
			$table->increments('id');
			$table->integer('user_id')->unsigned();
			$table->integer('role_id')->unsigned();
			$table->timestamps();
            $table->index(array('user_id', 'role_id'));
		});

		User::find(1)
			->roles()
			->attach(1);

		User::find(2)
			->roles()
			->attach(2);

        User::find(3)
			->roles()
			->attach(3);
	}

	public function down()
	{
		Schema::drop('users');
		Schema::drop('roles');
		Schema::drop('role_user');
	}
}